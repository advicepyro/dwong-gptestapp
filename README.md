# README #

An example web application built using Laravel 5.

# Usage #

For ideal results, extract the application into a Laravel Homestead environment. Once Homestead/Vagrant are setup, the following should be defined in .env:

* App was written with Mailgun integration in mind. Set config/mail.php accordingly and enter API keys into .env
* set MY_TEST_EMAIL to an email address of your choice

# Other Notes #

* The code's commit history contains old Mailgun keys. Don't use them; they're likely invalid by the time you read this.
* The example user for logging in is listed in the migrations: email is "user@example.com", password is "password"