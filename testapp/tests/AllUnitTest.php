<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AllUnitTest extends TestCase
{
	use DatabaseMigrations;

    /**
     * Tests adding a new entry, and verifying it exists in the database.
     *
     * @return void
     */
    public function test_add_new_entry()
    {
        $this->visit('/')
        	->type('Test User 9001', "username")
        	->type('testuser9001@example.com', "email")
        	->type('123-456-7890', "phone")
        	->press('Submit')
        	->seeInDatabase('entries', ["username" => "Test User 9001", "email" => "testuser9001@example.com", "phone" => "123-456-7890"]);
    }

    /**
    *   Tests submitting a blank form. We should see errors on the front page.
    *   
    *   @return void
    */
    public function test_blank_form_submission()
    {
        $this->visit('/')
            ->press('Submit')
            ->see('The username field is required. Please try again.')
            ->see('The email field is required. Please try again.')
            ->see('The phone field is required. Please try again.');
    }

    /**
     * Tests adding in an invalid phone number (larger than 32 chars)
     *
     * @return void
     */
    public function test_add_invalid_number()
    {
        $this->visit('/')
            ->type('Test User 9001', "username")
            ->type('testuser9001@example.com', "email")
            ->type('12345678901234567890123456789012345678901234567890', "phone")
            ->press('Submit')
            ->see("The phone may not be greater than 32 characters. Please try again.");
    }

    /**
    *   Tests modifying an entry.
    *   
    *   @return void
    */
    public function test_modify_an_entry()
    {
        $this->visit('/')
            ->type('Test User 9001', "username")
            ->type('testuser9001@example.com', "email")
            ->type('123-456-7890', "phone")
            ->press('Submit')
            ->seeInDatabase('entries', ["username" => "Test User 9001", "email" => "testuser9001@example.com", "phone" => "123-456-7890"])
            ->visit("/dashboard")
            ->type('user@example.com', "email")
            ->type('password', "password")
            ->press("Sign in")
            ->see("dashboard/view/1")
            ->visit("/dashboard/view/1")
            ->type("Test User 9002", "username")
            ->press("Update")
            ->see("Test User 9002");
    }

    /**
    *   Tests attempting to view an invalid entry ID.
    *   
    *   @return void
    */
    public function test_modify_invalid_entry()
    {
        $this->visit('/')
            ->type('Test User 9001', "username")
            ->type('testuser9001@example.com', "email")
            ->type('123-456-7890', "phone")
            ->press('Submit')
            ->seeInDatabase('entries', ["username" => "Test User 9001", "email" => "testuser9001@example.com", "phone" => "123-456-7890"])
            ->visit("/dashboard")
            ->type('user@example.com', "email")
            ->type('password', "password")
            ->press("Sign in")
            ->visit('/dashboard/view/1337')
            ->see("Key #1337 not found.");
    }

    /**
    *   Tests editing an entry with blank values. We should see errors, and nothing should be changed.
    *   
    *   @return void
    */
    public function test_modify_entry_incorrectly()
    {
        $this->visit('/')
            ->type('Test User 9001', "username")
            ->type('testuser9001@example.com', "email")
            ->type('123-456-7890', "phone")
            ->press('Submit')
            ->seeInDatabase('entries', ["username" => "Test User 9001", "email" => "testuser9001@example.com", "phone" => "123-456-7890"])
            ->visit("/dashboard")
            ->type('user@example.com', "email")
            ->type('password', "password")
            ->press("Sign in")
            ->see("dashboard/view/1")
            ->visit("/dashboard/view/1")
            ->type("", "username")
            ->type("", "email")
            ->type("", "phone")
            ->press("Update")
            ->see("The username field is required")
            ->see("The email field is required")
            ->see("The phone field is required")
            ->see("Test User 9001");
    }

    /**
    *   Tests deleting an entry.
    *   
    *   @return void
    */
    public function test_delete_entry()
    {
        $this->visit('/')
            ->type('Test User 9001', "username")
            ->type('testuser9001@example.com', "email")
            ->type('123-456-7890', "phone")
            ->press('Submit')
            ->seeInDatabase('entries', ["username" => "Test User 9001", "email" => "testuser9001@example.com", "phone" => "123-456-7890"])
            ->visit("/dashboard")
            ->type('user@example.com', "email")
            ->type('password', "password")
            ->press("Sign in")
            ->visit("/dashboard/delete/1")
            ->see("No entries in the database.");
    }

    /**
    *   Tests deleting an invalid entry.
    *   
    *   @return void
    */
    public function test_delete_invalid_entry()
    {
        $this->visit('/')
            ->type('Test User 9001', "username")
            ->type('testuser9001@example.com', "email")
            ->type('123-456-7890', "phone")
            ->press('Submit')
            ->seeInDatabase('entries', ["username" => "Test User 9001", "email" => "testuser9001@example.com", "phone" => "123-456-7890"])
            ->visit("/dashboard")
            ->type('user@example.com', "email")
            ->type('password', "password")
            ->press("Sign in")
            ->visit("/dashboard/delete/1337")
            ->see("Key #1337 not found.")
            ->see("Test User 9001");
    }

}
