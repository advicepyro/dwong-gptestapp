<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Entry;
use Illuminate\Http\Request;

// Front page...
Route::get('/', function (Request $request) {
    return view('index');
});

// Add a new entry via the front page
Route::post('/add', "FrontendController@addEntry");

// Login routines
Route::get("/auth/login", "Auth\AuthController@getLogin");
Route::post("/auth/login", "Auth\AuthController@postLogin");

// Dashboard views
Route::get("/dashboard", "DashboardController@main");
Route::get('/dashboard/view/{id}', "DashboardController@view");
Route::post('/dashboard/edit', "DashboardController@edit");
Route::get('/dashboard/delete/{id}', "DashboardController@delete");
Route::get('/dashboard/logout', "DashboardController@logout");