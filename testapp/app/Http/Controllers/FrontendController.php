<?php

namespace App\Http\Controllers;

use App\User;
use App\Entry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Jobs\SendNotification;

class FrontendController extends Controller {
	
	/**
	*	Adds a new entry to the database.
	*	
	*	@param	Request	request
	*	@return	Response
	*/
	public function addEntry(Request $request) {
		
		// Validate entered data
		$validator = Validator::make($request->all(), [
			'username' => 'required',
			'email' => 'required',
			'phone' => 'required|max:32',
		]);

		// Redirect back to frontpage and show errors if need be.
		if ($validator->fails()) {
			return back()->withInput()->withErrors($validator);
		}

		// Add a new entry to db
		$entry = new Entry;
		$entry->username = $request->username;
		$entry->email = $request->email;
		$entry->phone = $request->phone;
		$entry->save();

		// All saved? Now send an email via the queue
		$this->dispatch(new SendNotification($entry));
		
		return redirect('/?success=true');
	}
}