<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Entry;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DashboardController extends Controller
{
    /**
    *   Loads the main admin dashboard.
    *
    *   @param  Request request
    *   @return Response
    */
    public function main(Request $request) {

        // Make sure we have a logged in user, else kick them out
        if (!(Auth::check()))
            return redirect('/auth/login');

        return view('dashboard-main',
            [
                'entries' => Entry::orderBy('created_at', 'desc')->get()
            ]
        );
    }

    /**
    *   Views an entry and permits modifying values.
    *
    *   @param  Request request
    *   @param  id
    *   @return Response
    */
    public function view(Request $request, $id) {

        // Make sure we have a logged in user, else kick them out
        if (!(Auth::check()))
            return redirect('/auth/login');

        // Show error if we can't find the key
        try {
            $entry = Entry::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect('/dashboard')->withErrors(["Key #$id not found."]);
        }
        return view('dashboard-edit',
            [
                'entry' => $entry
            ]
        );
    }

    /**
    *   Processes an edit to an existing entry.
    *
    *   @param  Request request
    *   @return Response
    */
    public function edit(Request $request) {

        // Make sure we have a logged in user, else kick them out
        if (!(Auth::check()))
            return redirect('/auth/login');

        // Check if entry ID exists and if data is valid
        try {
            $entry = Entry::findOrFail($request->id);

            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'email' => 'required',
                'phone' => 'required|max:32',
            ]);
            if ($validator->fails()) {
                return redirect('/dashboard')->withErrors($validator); 
            }

            // Update the entry object
            $entry->username = $request->username;
            $entry->email = $request->email;
            $entry->phone = $request->phone;
            $entry->save();

            return redirect('/dashboard');

        } catch (ModelNotFoundException $e) {
            return redirect('/dashboard')->withErrors(["Key #$request->id not found."]);
        }
    }

    /**
    *   Deletes an existing entry.
    *
    *   @param  Request request
    *   @param  id
    *   @return Response
    */
    public function delete(Request $request, $id) {
        // Make sure we have a logged in user, else kick them out
        if (!(Auth::check()))
            return redirect('/auth/login');

        // Show error if we can't find the key
        try {
            $entry = Entry::findOrFail($id)->delete();
            return redirect('/dashboard');
        } catch (ModelNotFoundException $e) {
            return redirect('/dashboard')->withErrors(["Key #$id not found."]);
        }
    }

    /**
    *   Logs out the user.
    *
    *   @param  Request request
    *   @return Response
    */
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/dashboard');
    }
}


