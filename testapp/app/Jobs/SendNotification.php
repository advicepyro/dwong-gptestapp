<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entry;
use Mail;

class SendNotification extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $entry;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Entry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Adds a send email job to the queue.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send("emails.notification",
            [
                "username" => $this->entry->username,
                "email" => $this->entry->email,
                "phone" => $this->entry->phone,
            ],
            function ($message) {
                $message->from("noreply@example.com", "Test Domain Services");
                $message->to(env("MY_TEST_EMAIL"));
                $message->subject("A new entry has been entered!");
            }
        );
    }
}
