<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    // Define table used by this model
    protected $table = "entries";

    // Fields modifiable publically
    protected $fillable = ["username", "email", "phone"];
}
