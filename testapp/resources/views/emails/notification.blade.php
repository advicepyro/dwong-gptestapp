<html>
	<body>
		<p>Hello! You've just received a new entry.</p>
		<p><strong>Details:</strong></p>
		<ul>
			<li><strong>Name:</strong> {{ $username }}</li>
			<li><strong>Email address:</strong> {{ $email }}</li>
			<li><strong>Phone number:</strong> {{ $phone }}</li>
		</ul>
	</body>
</html>