<!-- View for editing an item in the dashboard -->

@extends('dashboard')
@section('content')
	<div class="main">
		<h1 class="page-header">Edit Item</h1>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<form action="/dashboard/edit" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="id" value="{{ $entry->id }}">
					<div class="form-group row">
						<div class="col-xs-4 col-sm-2">
							<label for="username">Username</label>
						</div>
						<div class="col-xs-8 col-sm-10">
							<input type="text" name="username" id="entry-username" class="form-control" placeholder="Name" required value="{{ $entry->username }}">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-4 col-sm-2">
							<label for="email">E-mail</label>
						</div>
						<div class="col-xs-8 col-sm-10">
							<input type="email" name="email" id="entry-email" class="form-control" placeholder="E-mail" required value="{{ $entry->email }}">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-4 col-sm-2">
							<label for="phone">Phone Number</label>
						</div>
						<div class="col-xs-8 col-sm-10">
							<input type="tel" name="phone" id="entry-phone" class="form-control" placeholder="Phone Number" maxlength="32" required value="{{ $entry->phone }}">
						</div>
					</div>
					<div class="btn-group pull-right" role="group">
						<a href="/dashboard" type="button" class="btn btn-default">Cancel</a>
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection