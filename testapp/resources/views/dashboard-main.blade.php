<!-- Main page for the dashboard -->

@extends('dashboard')
@section('content')
	<div class="main">
		@if (count($errors))
			@foreach($errors->all() as $error)
				<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;{{ $error }}</div>
			@endforeach
		@endif
		<h1 class="page-header">Dashboard</h1>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				@if(count($entries))
				<div class="table-responsive">
				<table class="table table-condensed table-hover">
					<thead>
						<tr>
							<th>Username</th>
							<th>E-mail</th>
							<th>Phone</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($entries as $entry)
							<tr>
								<td>{{ $entry->username }}</td>
								<td><a href="mailto:{{ $entry->email }}">{{ $entry->email }}</td>
								<td>{{ $entry->phone }}</td>
								<td>
									<a type="button" class="btn btn-default btn-xs" href="/dashboard/view/{{ $entry->id }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
									<a type="button" class="btn btn-default btn-xs delete" href="/dashboard/delete/{{ $entry->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				</div>
				@else
					<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;No entries in the database.</div>
				@endif
			</div>
		</div>
	</div>
@endsection