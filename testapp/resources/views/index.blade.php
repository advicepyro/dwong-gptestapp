<!-- Used for the Coming soon page only. -->

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="favicon.ico">

		<title>Coming Soon</title>

		<!-- Bootstrap core CSS -->
		<link href="/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="/css/cover.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					<div class="inner cover">
						<h1 class="cover-heading">Coming soon.</h1>
						<p class="lead">Enter your contact details below and we'll keep you in the loop.</p>
						<br/><br/>
						<form action="/add" method="post" class="lead form-horizontal row">
							{{ csrf_field() }}
							<div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4 col-md-6 col-md-offset-3">
								<div class="form-group">
									<input type="text" name="username" id="entry-username" class="form-control semitrans" placeholder="Name" required value="{{ old('username') }}">
								</div>
								<div class="form-group">
									<input type="email" name="email" id="entry-email" class="form-control semitrans" placeholder="E-mail" required value="{{ old('email') }}">
								</div>
								<div class="form-group">
									<input type="tel" name="phone" id="entry-phone" class="form-control semitrans" placeholder="Phone Number" maxlength="32" required value="{{ old('phone') }}">
								</div>
							<p class="lead">
								<button type="submit" class="btn btn-lg btn-default">Submit</a>
							</p>
							</div>
						</form>

						@if (count($errors))
							@foreach($errors->all() as $error)
								<p><strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;{{ $error }} Please try again.</strong></p>
							@endforeach
						@endif

						@if (isset($_GET['success']))
							<p><strong><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Success! We've received your info.</strong></p>
						@endif

					</div>

					<div class="mastfoot">
						<div class="inner">
							<p>Cover template for <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>. Background: <a href="https://interfacelift.com/wallpaper/details/1391/big_city_life.html" target="_blank">Big City Life.</a></p>
							<p><a href="/dashboard">[Admin login]</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
	</body>
</html>